<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>PT Lipuri Jagadh</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style1.css') }}" rel="stylesheet">

  <!-- Font Awesome 5 -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <style>
    #map {
      /* position: absolute; */
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 1;
    }

    div.scrollmenu {
      overflow: auto;
      white-space: nowrap;
    }

    div.scrollmenu div {
      display: inline-block;
    }

    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
                user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        padding: 10px 16px;
    }
        
    .btn-lg {
        font-size: 18px;
        line-height: 1.33;
        border-radius: 6px;
    }

    .btn-primary {
        color: #fff;
        background-color: #428bca;
        border-color: #357ebd;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active,
    .btn-primary.active,
    .open .dropdown-toggle.btn-primary {
        color: #fff;
        background-color: #3276b1;
        border-color: #285e8e;
    }

    /***********************
    OUTLINE BUTTONS
    ************************/

    .btn.outline {
        background: none;
        padding: 12px 22px;
    }
    .btn-primary.outline {
        border: 2px solid #0099cc;
        color: #0099cc;
    }
    .btn-primary.outline:hover, .btn-primary.outline:focus, .btn-primary.outline:active, .btn-primary.outline.active, .open > .dropdown-toggle.btn-primary {
        color: #33a6cc;
        border-color: #33a6cc;
    }
    .btn-primary.outline:active, .btn-primary.outline.active {
        border-color: #007299;
        color: #007299;
        box-shadow: none;
    }

    /***********************
    CUSTON BTN VALUES
    ************************/

    .btn {
        padding: 14px 24px;
        border: 0 none;
        font-weight: 700;
        letter-spacing: 1px;
        text-transform: uppercase;
    }
    .btn:focus, .btn:active:focus, .btn.active:focus {
        outline: 0 none;
    }
  </style>
</head>

<body>
    <!-- page 1 -->
    <section id="page1" class="clearfix">
        <div class="h-100" style="padding-left: 100px; padding-right: 50px; padding-top: 150px;">
            <div class="row justify-content-center align-self-center align-items-center" data-aos="fade-up">
                <div class="col-lg-7 intro-info order-md-first order-last" style="padding-top: 60px;" data-aos="zoom-in" data-aos-delay="100">
                    <h2>PROFIL PERUSAHAAN<br>PT LIPURI JAGADH</h2>
                    <div>
                        <a href="/login" class="btn btn-primary btn-lg outline">Login</a>
                        <a href="#page2" class="btn btn-primary btn-lg outline">Lihat Profil</a>
                    </div>
                </div>
      
                <div class="col-lg-5 intro-img order-md-last order-first" data-aos="fade-up" data-aos-delay="300">
                    <div class="row counters" data-aos="fade-up" data-aos-delay="100">
                        <div class="col-lg-6 col-6 text-center">
                            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">100</span>
                            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Partner</p>
                        </div>
                        <div class="col-lg-6 col-6 text-center">
                            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">300</span>
                            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Perjanjian</p>
                        </div>
                        <div class="col-lg-12 col-12 text-center">
                            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">3000</span>
                            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Pegawai</p>
                          </div>                  
                    </div>
                </div>
            </div>
      
        </div>
    </section>

    <!-- page 2 -->
    <section id="page2" class="clearfix">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 100px; padding-bottom: 100px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-5 col-md-6">
                <div class="about-img" data-aos="fade-right" data-aos-delay="100">
                    <img src="{{ asset('assets/img/team-1.jpg') }}" width="490" alt="" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                </div>
            </div>

            <div class="col-lg-7 col-md-6">
                <div data-aos="fade-left" data-aos-delay="100">
                    <h2>Jabatan</h2>
                    <h3>Nama, S.T., M.T.</h3>
                    <br>
                    <p>
                        Rasa Syukur kami panjatkan kepada Tuhan yang Esa / Idha Sang Hyang Widhi Wasa karena atas berkatNya lah kami bisa ada seperti sekarang ini.
                    </p>
                    <p>
                        Diawali dengan rasa keinginan untuk bisa menolong sesama liwat membuka lowongan kerja dan rasa prihatin akan lingkungan sekitar, 
                        maka dengan modal keberanian dan pengalaman kerja yang kami miliki dimana di tahun 2000 maka kami membuka usaha dagang di sebuah Desa kecil di kecamatan Kediri Kabupaten Tabanan Bali. 
                    </p>
                    <p>
                        Kami lahir di keluarga pedagang dimana orang tua kami adalah pedagang hasil bumi di dalam pasar desa.Kami terus berusaha sekuat tenaga, focus, kerja keras, berinovasi, 
                        pengembangan sumber daya manusia dan mengadopsi tatanan managemen modern maka tahun 2003 kami berubah menjadi CV dan kemudian tahun 2010 menjadi PT. Lipuri Jagadh. 
                    </p>
                    <p>
                        Layanan adalah menjadi ujung tombak usaha kami dengan terus berbenah di segala aspek sehingga bisa mewujudkan layanan yang prima kepada para pelanggan, 
                        PT. Lipuri Jagadh percaya dengan selalu mengedepankan sikap jujur , saling menghargai dan saling menghormati yang adalah faktor yang membuat usaha kami unggul. 
                    </p>
                    <p>
                        Profesional, menguntungkan, berkembang bersama dan berkelanjutan adalah keinginan kami yang selalu kami kawal bersama dan merubah dari moto menjadi prilaku kerja / etos kerja. 
                        Dengan bekerja sama dengan PT. Lipuri Jagadh adalah pilihan yang tepat dalam membuat usaha anda maju dan berkelanjutan.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- page 3 -->
    <section id="page3" class="clearfix" style="background-color: #f0f8ff;">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 100px; padding-bottom: 100px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-7 col-md-6">
                <div data-aos="fade-left" data-aos-delay="100">
                    <h2>Riwayat Singkat Perusahaan</h2>
                    <br>
                    <p>
                        Awal pejalanan kami adalah dengan didirikan sebuah usaha dagang dan jasa pertama didirikan oleh I Wayan Sukarma di tahun 2000 di sebuah desa kecil, 
                        kecamatan Kediri kabupaten Tabanan –Bali dimana desa kami letaknya hanya 15 menit dari Pura Tanah Lot yang sudah terkenal akan keindahannya sampai mancanegara.
                    </p>
                    <p>
                        Tahun 2003 kami berubah badan hukum dari UD menjadi CV dan kami menambah wilayah 
                        usaha dan sub bidang usaha diantara nya :
                    </p>
                    <ul>
                        <li>Usaha jasa penyewaan Kendaraan</li>
                        <li>Jasa cleaning service</li>
                        <li>Jasa konstruksi</li>
                        <li>Lanscaping</li>
                        <li>Jasa Sumber Daya Manusia</li>
                    </ul>
                    <p>
                        2010 Syukur berkat doa dan kerja keras team kami berubah badan hukum dari CV menjadi 
                        PT dan juga namanya mengalami perubahan menjadi PT. Lipuri Jagadh dengan menambah 
                        jenis atau sub bidang usaha di Jasa tenaga kerja dan jasa Keamanan. 
                    </p>
                    <p>
                        Dalam menjalankan usaha kami diatas dimana kami sudah mendapatkan ijin dari Mabes 
                        Polri Jakarta. Ligapuri group mempunyai komitmen akan selalu berbenah secara Internal 
                        maupun External dengan jalan mengutakan pelayanan , image , pengembangan suberdaya 
                        manusia , pengembangan management modern dan team work. 
                    </p>
                </div>
            </div>

            <div class="col-lg-5 col-md-6">
                <div class="about-img" data-aos="fade-right" data-aos-delay="100">
                    <img src="{{ asset('assets/img/team-1.jpg') }}" width="490" alt="" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                </div>
            </div>
        </div>
    </section>

    <!-- page 4 -->
    <section id="page4">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 50px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-6 col-md-6">
                <div class="box" data-aos="fade-left" data-aos-delay="100">
                    <div class="icon" style="background: #fceef3;"><i class="fas fa-building" style="color: #ff689b;"></i></div>
                    <h4 class="title">Visi</h4>
                    <p class="description">
                        Berkembang menjadi perusahaan pilihan , unggul serta bisa di percaya untuk penyedia jasa dan product transportasi.
                    </p>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="box" data-aos="fade-left" data-aos-delay="100">
                    <div class="icon" style="background: #fceef3;"><i class="fas fa-building" style="color: #ff689b;"></i></div>
                    <h4 class="title">Misi</h4>
                    <ul>
                        <li class="description">Sanggup memberikan nilai maksimum ke seluruh stakeholder dan terus tumbuh secara berkesinambungan.</li>
                        <li class="description">Mengembangkan sumber daya manusia yang siap dalam persaingan globaL, membangun managemen modern serta menciptakan lapangan pekerjaan.</li>
                    </ul>     
                </div>
            </div>
        </div>
    </section>

    <!-- page 5 -->
    <section id="page5" class="clearfix" style="background-color: #f0f8ff;">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 100px; padding-bottom: 100px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-12 col-md-12">
                <div data-aos="fade-left" data-aos-delay="100">
                    <center><h2>Tentang Kami</h2></center>
                    <br><br>
                    <p>
                        Kami adalah perusahaan penyedia jasa dengan usaha yang mencakup :
                    </p>
                    <ul>
                        <li>Usaha jasa penyewaan Kendaraan</li>
                        <li>Jasa cleaning service</li>
                        <li>Jasa Kontruksi</li>
                        <li>Jasa Sumber Daya Manusia</li>
                    </ul>
                    <p>
                        Sebagai bagian perusahaan jasa dan transportasi kami memulai usaha tahun 2000 dan terus berkembang dimana hingga sekarang kami telah mengelola dan melayani.
                    </p>
                    <p>
                        Diadalam perjalanan kami dan juga sebagai bagian dari wujud komitmen kami kepada para pelanggan, kami tetap menjaga dan menjadi partner bisnis yang saling menguntungkan, berkembang bersama serta berkelanjutan. Kami selalu mengutamakan keselamatan dan kemyamanan dengan menjalan qaulity control.
                    </p>
                    <p>
                        Kami memiliki tim managemen yang sangat loyal dengan pengalaman yang diperoleh dari semua hotel International Chance terkemuka di Bali. Lebih penting lagi, tim pernah mengelola beberapa resor dan grup hotel besar serta Luxury Villas. Oleh karena itu kami sangat paham akan dinamika lingkungan di mana kami berada.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- page 6 -->
    <section id="page6">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 50px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-6 col-md-12">
                <div class="box" data-aos="fade-right" data-aos-delay="100">
                    <div class="icon" style="background: #fceef3;"><i class="fas fa-building" style="color: #ff689b;"></i></div>
                    <h4 class="title">Nilai Budaya</h4>
                    <p class="description">
                        Memperkaya diri kita dengan budaya lokal, Bali adalah tempat dimana perusahaan dan pelanggan kami berada tentu sudah menjadi kewajiban bagi 
                        seluruh anggota kami untuk memperkaya diri kita dengan dengan budaya lokal seperti Pakian dan penampilan, bahasa dan simbul simbul, 
                        sikap dan tindak tanduk kami yang mana akan meningkatkan citra perusahaan kami
                    </p>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="box" data-aos="fade-left" data-aos-delay="100">
                    <div class="icon" style="background: #fceef3;"><i class="fas fa-building" style="color: #ff689b;"></i></div>
                    <h4 class="title">Nilai-Nilai Kebersamaan</h4>
                    <p class="description">
                        Perusahaan kami yakin dengan bersama kami bisa, menjalankan bisnis dengan saling menghormati serta tanggung jawab terhadap rekan bisnis, 
                        karyawan dan lingkungan sekitar.
                    </p>     
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="box" data-aos="fade-right" data-aos-delay="100">
                    <div class="icon" style="background: #fceef3;"><i class="fas fa-building" style="color: #ff689b;"></i></div>
                    <h4 class="title">Intergritas</h4>
                    <p class="description">
                        Mempunyai komitmen yang besar untuk mengelola perusahaan yang benar dengan sikap kebersamaan, menjungjung tinggi standar etika serta tunduk pada 
                        segala peraturan dan hukum yang berlaku bagi seluruh anggota perusahaan sebagai kunci kesuksesan organisasi.
                    </p>   
                </div>
            </div>
        </div>
    </section>

    <!-- page 7 -->
    <section id="page7" class="clearfix">
        <div class="row" style="padding-left: 50px; padding-right: 50px; padding-top: 100px; padding-bottom: 100px; margin: 0px;" data-aos="fade-up">
            <div class="col-lg-5 col-md-6">
                <div class="about-img" data-aos="fade-right" data-aos-delay="100">
                    <img src="{{ asset('assets/img/team-1.jpg') }}" width="490" alt="" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
                </div>
            </div>

            <div class="col-lg-7 col-md-6">
                <div data-aos="fade-left" data-aos-delay="100">
                    <h2>PT LIPURI JAGADH PEDULI</h2>
                    <br>
                    <p>
                        Di dalam rejeki kita ada hak hak orang lain, kami sangat yakin dan percaya dengan sepenggal kata kata tersebut diatas.
                    </p>
                    <p>
                        Kami di Bali punya philosopi hidup bersifat universal yang kita sebut dengan konsep Tri Hita Karana yaitu sebuah pandangan dan konsep keseimbangan 
                        hidup yaitu keseimbangan hidup antara Manusia dengan Alam, Manusia dengan Manusia dan Manusia dengan Tuhan.
                    </p>
                    <p>
                        PT. Lipuri Jagadh tidak hanya semata mata mencari keuntungan untuk perusahaan saja, kami sangat peduli dengan sangat mendukung kegiatan kegiatan sosial lingkungan sekitar.
                    </p>
                    <br>
                    <p class="text-center">
                        <b style="font-size: 20px;">Kegiatan antara Manusia dengan Alam</b>, 
                        <br>
                        kami memberi dukungan kepada masyarakat didalam meningkatkan kesadaran masyarakat akan pentingnya menjaga lingkungan 
                        hidup terutama di mana kita berada juga untuk Bali dan Nasional, secara aktip ikut berpartisipasi program 
                        pemerintah didalam menjaga alam seperti penanaman pohon, menjaga kebersihan dan menjaga sumber sumber alam.
                    </p>
                    <p class="text-center">
                        <b style="font-size: 20px;">Kegiatan antara Manusia dan Mansia</b>, 
                        <br>
                        Secara berkesinambungan tetap menyisihkan keuntungan untuk saudara kita yang belum beruntung / miskin untuk kegiatan 
                        seperti pendidikan, kesehatan, bedah rumah dan kegiatan kemanusian lainnya.
                    </p>
                    <p class="text-center">
                        <b style="font-size: 20px;">Kegiatan antara Manusia dengan Tuhan</b>, 
                        <br>
                        di Bali aktipitas budaya dan hunbungan manusia dengan Tuhan sudah menjadi bagian hidup orang Bali kususnya yang beragama 
                        Hindu dan tiada hari yang tidak ada kegiatan budaya / keagamaan dan inilah yang menjadi arwahnya Bali sehinga 
                        banyak dikunjungi wisatawan domestik dan asing. Kami selalu aktip berpartisipasi kegiatan adat dan budaya
                    </p>
                </div>
            </div>
        </div>
    </section>

     footer -->
    <footer id="footer" class="clearfix">
        <div class="footer-top">
            <div class="container">
                <div class="row" style="background: #f5f8fdaf; padding: 40px;">
                    <div class="col-lg-7 col-md-12">
                        <div class="footer-info">
                            <h3>PT LIPURI JAGADH</h3>
                            <h4>Partners</h4>
                            <ul style="columns: 2; -webkit-columns: 2; -moz-columns: 2;">
                                <li>Pemerintah Daerah Kabupaten Tabanan Bali</li>
                                <li>Pemerintah Kabuptaen Jemberana Bali</li>
                                <li>Pemerintah Daerah Bali </li>
                                <li>Rumah Sakit Indra Denpasar Bali</li>
                                <li>Rumah sakit umum Jemberana Bali</li>
                                <li>Rumah Sakit Mangunpura Badung Bali</li>
                                <li>Rumah Sakit Umum Jiwa Bangli Bali</li>
                                <li>Rumah Sakit Bali Mandara Bali</li>
                                <li>Kantor Keuangan Bali</li>
                                <li>Kantor Bank Indonesia Bali</li>
                                <li>Kantor OJK Bali</li>
                                <li>PT PLN</li>
                                <li>PT POS</li>
                                <li>PT BPD Bali</li>
                                <li>PT Agung Toyota</li>
                                <li>PT Angkasa Pura 1</li>
                                <li>PT ACS</li>
                                <li>PT BPR Sutra</li>
                                <li>PT Auto Bagus</li>
                                <li>PT Tangkas Perkasa</li>
                                <li>BNR Tanah Lot Bali</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-12">
                        <div class="footer-links">
                            <h4>Kontak Kami</h4>
                            <p>
                                <strong>Kantor :</strong> Jalan Melati No 6 Banjar Puseh Kediri Tabanan Bali <br>
                                <strong>Workshop :</strong> Jalan Ir. Sukarno No. 77 Y <br>
                                <strong>Telp :</strong>  ( 0361 ) 4790579 <br>
                                <strong>Fax :</strong> ( 0361 ) 4790578<br>
                                <strong>Email :</strong> liga_puri01@yahoo.com<br>
                            </p>
                            <p>
                                <strong>I Wayan Sukarma :</strong> 08123952788 <br>
                                <strong>Novi Rahayu :</strong> 085737408071 <br>
                                <strong>Gede Suarsa (Business Development) :</strong> +62 00 000 000 000<br>
                                <strong>Email :</strong> gede.suars4@gmail.com<br>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </footer>

  <a href="#" class="back-to-top"><i class="fas fa-chevron-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
    <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/counterup/counterup.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>

    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>