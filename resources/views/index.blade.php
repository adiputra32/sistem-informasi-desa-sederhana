<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Desa Kuwum</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/venobox/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  <!-- Font Awesome 5 -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

  <!-- Leaflet -->
  <link crossorigin="" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" />
  <script crossorigin="" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"></script>
  <link crossorigin="" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" rel="stylesheet">
  <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/leaflet.fullscreen.css' rel='stylesheet' />
  <script src="https://unpkg.com/leaflet-kmz@latest/dist/leaflet-kmz.js"></script>
  <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-fullscreen/v1.0.1/Leaflet.fullscreen.min.js'></script>

  <style>
    #map {
      /* position: absolute; */
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 1;
    }

    div.scrollmenu {
      overflow: auto;
      white-space: nowrap;
    }

    div.scrollmenu div {
      display: inline-block;
    }
  </style>

  <!-- =======================================================
  * Template Name: Rapid - v2.2.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <div id="topbar" class="d-none d-lg-flex align-items-end fixed-top topbar-transparent">
    <div class="container d-flex justify-content-end">
      <div class="social-links">
        <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
        <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
        <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
        <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
      </div>
    </div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-transparent">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">Desa Kuwum</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo mr-auto"><img src="{{ asset('assets/img/logo.png') }}" alt="" class="img-fluid"></a> -->

      <nav class="main-nav d-none d-lg-block">
        <ul>
          <li class="active"><a href="/">Home</a></li>
          <li class="drop-down"><a href="#about">Profil Desa</a>
            <ul>
              <li><a href="#">Profil Wilayah Desa</a></li>
              <li><a href="#">Sejarah Desa</a></li>
              <li><a href="#">Visi dan Misi</a></li>
              <li><a href="#">Pemerintah Desa</a></li>
              <li class="drop-down"><a href="#">Data Desa</a>
                <ul>
                  <li><a href="#">Data Wilayah Administratif</a></li>
                  <li><a href="#">Data Pekerjaan</a></li>
                  <li><a href="#">Data Agama</a></li>
                  <li><a href="#">Data Warga Negara</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="drop-down"><a href="#services">Layanan Desa</a>
            <ul>
              <li><a href="#">Layanan Promosi Warga</a></li>
              <li><a href="#">Layanan Kesehatan</a></li>
              <li><a href="#">Layanan Posyandu</a></li>
              <li><a href="#">Layanan Informasi Publik</a></li>
              <li><a href="#">Layanan Surat-Surat</a></li>
            </ul>
          </li>
          <li><a href="#why-us">Sebaran Lokasi</a></li>
          <li><a href="#features">Pengaduan</a></li>
        </ul>
      </nav><!-- .main-nav-->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center" data-aos="fade-up">
        <div class="col-md-6 intro-info order-md-first order-last" data-aos="zoom-in" data-aos-delay="100">
          <h2>Sistem Informasi<br>Desa <span>Kuwum</span></h2>
          <div>
            <a href="#about" class="btn-get-started scrollto">Masuk sebagai Warga</a>
          </div>
        </div>

        <!-- <div class="col-md-6 intro-img order-md-last order-first" data-aos="zoom-out" data-aos-delay="200">
          <img src="{{ asset('assets/img/smart-city.png') }}" alt="" class="img-fluid">
        </div> -->
      </div>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Info Corona Section ======= -->
    <section class="why-us" style="padding-top: 60px;">
      <div class="container">
        <div class="row counters" data-aos="fade-up" data-aos-delay="100">
  
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">100</span>
            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Positif</p>
          </div>
  
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">0</span>
            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Dalam Perawatan</p>
          </div>
  
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">100</span>
            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Sembuh</p>
          </div>
  
          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up" style="font-family: 'Montserrat', sans-serif; font-weight: bold; font-size: 48px; display: block; color: #555186;">0</span>
            <p style="padding: 0; margin: 0 0 20px 0; font-family: 'Montserrat', sans-serif; font-size: 14px; color: #8a87b6;">Meninggal</p>
          </div>
  
        </div>
  
      </div>

    </section><!-- End Info Corona Section -->

    <!-- ======= Kepala Desa Section ======= -->
    <section id="about" class="about">

      <div class="container" data-aos="fade-up">
        <div class="row">

          <div class="col-lg-5 col-md-6">
            <div class="about-img" data-aos="fade-right" data-aos-delay="100">
              <img src="{{ asset('assets/img/about-img.jpg') }}" alt="">
            </div>
          </div>

          <div class="col-lg-7 col-md-6">
            <div class="about-content" data-aos="fade-left" data-aos-delay="100">
              <h2>Kepala Desa</h2>
              <h3>I Putu Kadek Komang Ketut, S.Sos</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
              <p>Aut dolor id. Sint aliquam consequatur ex ex labore. Et quis qui dolor nulla dolores neque. Aspernatur consectetur omnis numquam quaerat. Sed fugiat nisi. Officiis veniam molestiae. Et vel ut quidem alias veritatis repudiandae ut fugit. Est ut eligendi aspernatur nulla voluptates veniam iusto vel quisquam. Fugit ut maxime incidunt accusantium totam repellendus eum error. Et repudiandae eum iste qui et ut ab alias.</p>
              <ul>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ion-android-checkmark-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </section><!-- End Kepala Desa Section -->

    <!-- ======= Layanan Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h3>Layanan Desa</h3>
          <p>Layanan-layanan yang disediakan oleh Desa Kuwum</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-aos="zoom-in" data-aos-delay="100">
            <div class="box">
              <div class="icon" style="background: #fceef3;"><i class="fas fa-store" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="/login">Layanan Promosi Warga</a></h4>
              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4" data-aos="zoom-in" data-aos-delay="200">
            <div class="box">
              <div class="icon" style="background: #fff0da;"><i class="fas fa-user-md" style="color: #e98e06;"></i></div>
              <h4 class="title"><a href="/login">Layanan Kesehatan</a></h4>
              <p class="description">Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat tarad limino ata</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4" data-aos="zoom-in" data-aos-delay="300">
            <div class="box">
              <div class="icon" style="background: #e6fdfc;"><i class="fas fa-baby" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="/login">Layanan Posyandu</a></h4>
              <p class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 wow" data-aos="zoom-in" data-aos-delay="100">
            <div class="box">
              <div class="icon" style="background: #eafde7;"><i class="fas fa-newspaper" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="/login">Layanan Informasi Publik</a></h4>
              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4" data-aos="zoom-in" data-aos-delay="200"">
            <div class=" box">
              <div class="icon" style="background: #e1eeff;"><i class="fas fa-file-alt" style="color: #2282ff;"></i></div>
              <h4 class="title"><a href="/login">Layanan Surat-Surat</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>
          </div>

      </div>

      </div>
    </section><!-- End Layanan Section -->

    <!-- ======= Sebaran Lokasi Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container-fluid" data-aos="fade-up">

        <header class="section-header">
          <h3>Sebaran Lokasi</h3>
          <p>Sebaran lokasi-lokasi yang ada di Desa Kuwum</p>
        </header>

        <div class="row">

          <div class="col-lg-8" data-aos="zoom-in" data-aos-delay="100">
            <div class="why-us-img">
              <div id="map" style="height: 500px;">
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="why-us-content">
              <h4>Keterangan Simbol</h4>

              <hr>

              <div class="features clearfix" data-aos="fade-up" data-aos-delay="100">
                <i class="fas fa-hospital" style="color: #f058dc;"></i>
                <h5 style="padding-left: 55px;"> Rumah Sakit</h5>
              </div>

              <div class="features clearfix" data-aos="fade-up" data-aos-delay="200">
                <i class="fas fa-school" style="color: #ffb774;"></i>
                <h5 style="padding-left: 55px;"> Sekolah</h5>
              </div>

              <div class="features clearfix" data-aos="fade-up" data-aos-delay="300">
                <i class="fas fa-building" style="color: #589af1;"></i>
                <h5 style="padding-left: 55px;"> Kantor Polisi</h5>
              </div>

              <div class="features clearfix" data-aos="fade-up" data-aos-delay="300">
                <i class="fas fa-landmark" style="color: #589af1;"></i>
                <h5 style="padding-left: 55px;"> Kantor Desa</h5>
              </div>

              <div class="features clearfix" data-aos="fade-up" data-aos-delay="300">
                <i class="fas fa-clinic-medical" style="color: #589af1;"></i>
                <h5 style="padding-left: 55px;"> Puskesmas</h5>
              </div>

            </div>

          </div>

        </div>

      </div>

    </section><!-- End Sebaran Lokasi Section -->

    <!-- ======= Hubungi Kami Section ======= -->
    <section id="call-to-action" class="call-to-action">
      <div class="container" data-aos="zoom-out">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Butuh Bantuan?</h3>
            <p class="cta-text"> Apakah anda butuh bantuan dari kami? Klik tombol hubungi untuk menghubungi nomor Desa Kuwum.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Hubungi Desa</a>
          </div>
        </div>

      </div>
    </section><!--  End Hubungi Kami Section -->

    <!-- ======= Pengaduan Section ======= -->
    <section id="features" class="features">
      <div class="container" data-aos="fade-up">

        <div class="row feature-item">
          <div class="col-lg-6" data-aos="fade-right" data-aos-delay="100">
            <img src="{{ asset('assets/img/complain.png') }}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0" data-aos="fade-left" data-aos-delay="150">
            <h4>Formulir Pengaduan Masyarakat Desa Kuwum</h4>
            <div class="form">
              <form action="/" method="post" role="form" class="php-email-form">
                <div class="form-group">
                  <span style="font-family: 'Montserrat', sans-serif; font-weight: 400;">Nama</span>
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nama" data-rule="minlen:4" data-msg="Minimal 4 huruf" required />
                  <div class="validate"></div>
                </div>

                <div class="form-group">
                  <span style="font-family: 'Montserrat', sans-serif; font-weight: 400;">Perihal</span>
                  <input type="text" class="form-control" name="perihal" id="perihal" placeholder="Perihal" required />
                  <div class="validate"></div>
                </div>

                <div class="form-group">
                  <span style="font-family: 'Montserrat', sans-serif; font-weight: 400;">Tanggal Kejadian</span>
                  <input type="date" class="form-control" name="tanggal" id="tanggal" placeholder="Tanggal" required />
                  <div class="validate"></div>
                </div>
                
                <div class="form-group">
                  <span style="font-family: 'Montserrat', sans-serif; font-weight: 400;">Isi Pengaduan</span>
                  <textarea class="form-control" name="pengaduan" rows="5" placeholder="Isi Pengaduan" required></textarea>
                  <div class="validate"></div>
                </div>

                <div class="mb-3">
                  <div class="loading" style="display: none; background: #fff; text-align: center; padding: 15px;">Loading</div>
                  <div class="error-message" style="display: none; color: #fff; background: #ed3c0d; text-align: left; padding: 15px; font-weight: 600;"></div>
                  <div class="sent-message" style="display: none; color: #fff; background: #18d26e; text-align: center; padding: 15px; font-weight: 600;">Pengaduan anda telah dikirim! Terima Kasih.</div>
                </div>

                <div class="text-center"><button type="submit" title="Kirim Pengaduan" style="background: #1bb1dc; border: 0; border-radius: 3px; padding: 8px 30px; color: #fff; transition: 0.3s;">Kirim</button></div>
              </form>
            </div>
            
          </div>
        </div>

      </div>
    </section><!-- End Pengaduan Section -->

    <!-- ======= Promosi Warga Section ======= -->
    <section id="portfolio" class="portfolio section-bg">
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <h3 class="section-title">Promosi Warga</h3>
        </header>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">Semua</li>
              <li data-filter=".filter-app">Jenis 1</li>
              <li data-filter=".filter-card">Jenis 2</li>
              <li data-filter=".filter-web">Jenis 3</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/app1.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 1</a></h4>
                <p>App</p>
                <div>
                  <a href="assets/img/portfolio/app1.jpg" data-gall="portfolioGallery" title="App 1" class="link-preview venobox"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/web3.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Web 3</a></h4>
                <p>Web</p>
                <div>
                  <a href="assets/img/portfolio/web3.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Web 3"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/app2.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 2</a></h4>
                <p>App</p>
                <div>
                  <a href="assets/img/portfolio/app2.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="App 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/card2.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Card 2</a></h4>
                <p>Card</p>
                <div>
                  <a href="assets/img/portfolio/card2.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Card 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/web2.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Web 2</a></h4>
                <p>Web</p>
                <div>
                  <a href="assets/img/portfolio/web2.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Web 2"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/app3.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">App 3</a></h4>
                <p>App</p>
                <div>
                  <a href="assets/img/portfolio/app3.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="App 3"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/card1.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Card 1</a></h4>
                <p>Card</p>
                <div>
                  <a href="assets/img/portfolio/card1.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Card 1"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/card3.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Card 3</a></h4>
                <p>Card</p>
                <div>
                  <a href="assets/img/portfolio/card3.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Card 3"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="{{ asset('assets/img/portfolio/web1.jpg') }}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="portfolio-details.html">Web 1</a></h4>
                <p>Web</p>
                <div>
                  <a href="assets/img/portfolio/web1.jpg" class="link-preview venobox" data-gall="portfolioGallery" title="Web 1"><i class="ion ion-eye"></i></a>
                  <a href="portfolio-details.html" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Promosi Warga Section -->

    <!-- ======= Berita Section ======= -->
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">
        <div class="section-header">
          <h3>Berita</h3>
          <p>Berita Terbaru Desa Kuwum</p>
        </div>

        <div class="scrollmenu" data-aos-delay="100">
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-1.jpeg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-2.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-3.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-4.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-1.jpeg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-2.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-3.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
  
            <div style="width: 350px; height: 300px; margin-left: 15px; margin-right: 15px;" data-aos="fade-up">
              <div class="member">
                <img src="{{ asset('assets/img/berita-4.jpg') }}" style="width: auto; height: 300px;" class="img-fluid" alt="">
                <div class="member-info" onclick="location.href='/'" style="cursor: pointer;">
                  <div class="member-info-content" style="margin-bottom: 20px;">
                    <h4>Contoh Berita Satu</h4>
                    <span>31 Oktober 2020</span>
                    <div class="social">
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        

      </div>
    </section><!-- End Berita Section -->

    <!-- ======= Agenda Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container" data-aos="fade-up">
        <header class="section-header">
          <h3>Agenda Desa</h3>
          <p>Daftar Agenda Desa Kuwum</p>
        </header>

        <ul id="faq-list" data-aos="fade-up" data-aos-delay="100" style="height: 390px; overflow: auto;">
          <li>
            <a data-toggle="collapse" class="collapsed" href="#faq1">Senin, 2 November 2020 - Odalan di Pura Puseh <i class="ion-android-remove"></i></a>
            <div id="faq1" class="collapse" data-parent="#faq-list">
              <p>
                Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Selasa, 3 November 2020 - Posyandu <i class="ion-android-remove"></i></a>
            <div id="faq2" class="collapse" data-parent="#faq-list">
              <p>
                Perihal : Posyandu <br>
                Hari/Tangal : Selasa, 3 November 2020 <br>
                Tempat : Puskesmas 1 <br>
                Keterangan : - <br>
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Rabu, 4 November 2020 - Rapat Karang Taruna <i class="ion-android-remove"></i></a>
            <div id="faq3" class="collapse" data-parent="#faq-list">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Kamis, 5 November 2020 - Rapat Desa <i class="ion-android-remove"></i></a>
            <div id="faq4" class="collapse" data-parent="#faq-list">
              <p>
                Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Jumat, 6 November 2020 - Lomba Mancing <i class="ion-android-remove"></i></a>
            <div id="faq5" class="collapse" data-parent="#faq-list">
              <p>
                Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq6" class="collapsed">Sabtu, 7 November 2020 - Kerja Bakti <i class="ion-android-remove"></i></a>
            <div id="faq6" class="collapse" data-parent="#faq-list">
              <p>
                Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" class="collapsed" href="#faq1">Senin, 2 November 2020 - Odalan di Pura Puseh <i class="ion-android-remove"></i></a>
            <div id="faq1" class="collapse" data-parent="#faq-list">
              <p>
                Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">Selasa, 3 November 2020 - Posyandu <i class="ion-android-remove"></i></a>
            <div id="faq2" class="collapse" data-parent="#faq-list">
              <p>
                Perihal : Posyandu <br>
                Hari/Tangal : Selasa, 3 November 2020 <br>
                Tempat : Puskesmas 1 <br>
                Keterangan : - <br>
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">Rabu, 4 November 2020 - Rapat Karang Taruna <i class="ion-android-remove"></i></a>
            <div id="faq3" class="collapse" data-parent="#faq-list">
              <p>
                Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">Kamis, 5 November 2020 - Rapat Desa <i class="ion-android-remove"></i></a>
            <div id="faq4" class="collapse" data-parent="#faq-list">
              <p>
                Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">Jumat, 6 November 2020 - Lomba Mancing <i class="ion-android-remove"></i></a>
            <div id="faq5" class="collapse" data-parent="#faq-list">
              <p>
                Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq6" class="collapsed">Sabtu, 7 November 2020 - Kerja Bakti <i class="ion-android-remove"></i></a>
            <div id="faq6" class="collapse" data-parent="#faq-list">
              <p>
                Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Nibh tellus molestie nunc non blandit massa enim nec.
              </p>
            </div>
          </li>

        </ul>

      </div>
    </section><!-- End Agenda Section -->

    <!-- ======= Logo Section ======= -->
    <section id="clients" class="clients">
      <div class="container" data-aos="zoom-in">

        <header class="section-header">
          <h3>Pemerintah Desa Kuwum</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="{{ asset('assets/img/clients/client-1.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-2.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-3.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-4.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-5.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-6.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-7.png') }}" alt="">
          <img src="{{ asset('assets/img/clients/client-8.png') }}" alt="">
        </div>

      </div>
    </section><!-- End Logo Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-9">

            <div class="row">

              <div class="col-sm-6">

                <div class="footer-info">
                  <h3>Desa Kuwum</h3>
                  <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
                </div>

              </div>

              <div class="col-sm-3">
                <div class="footer-links">
                  <h4>Layanan Desa</h4>
                  <ul>
                    <li><a href="#services">Layanan Promosi Warga</a></li>
                    <li><a href="#services">Layanan Kesehatan</a></li>
                    <li><a href="#services">Layanan Posyandu</a></li>
                    <li><a href="#services">Layanan Informasi Publik</a></li>
                    <li><a href="#services">Layanan Surat-Surat</a></li>
                  </ul>
                </div>

              </div>

              <div class="col-sm-3">
                <div class="footer-links">
                  <h4>Profil Desa</h4>
                  <ul>
                    <li><a href="#services">Profil Wilayah Desa</a></li>
                    <li><a href="#services">Sejarah Desa</a></li>
                    <li><a href="#services">Visi dan Misi</a></li>
                    <li><a href="#services">Pemerintah Desa</a></li>
                  </ul>
                </div>

              </div>

            </div>

          </div>

          <div class="col-lg-3">

            <div class="footer-links">
              <h4>Kontak Kami</h4>
              <p>
                Jalan Kuwum, Kediri <br>
                Tabanan, Bali 535022<br>
                Indonesia <br>
                <strong>Telepon:</strong> +62 00 000 000 000<br>
                <strong>Email:</strong> desaKuwum@tabanankab.go.id<br>
              </p>
            </div>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fab fa-instagram"></i></a>
              <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
            </div>

          </div>

        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fas fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>
  <script src="{{ asset('assets/vendor/isotope-layout/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/venobox/venobox.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/owl.carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/waypoints/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/js/main.js') }}"></script>

  <script>
    var popup = L.popup();
    var mymap = L.map('map').setView([-8.4212042, 115.1195972], 11);
    var layerGroup = L.layerGroup().addTo(mymap);
    var kmzParser = null;
    var markers = [];

    $(document).ready(function(){
      // add map into layer
      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1Ijoid2lkaWFuYXB3IiwiYSI6ImNrNm95c2pydjFnbWczbHBibGNtMDNoZzMifQ.kHoE5-gMwNgEDCrJQ3fqkQ'
      }).addTo(mymap);

      // fullscreen button
      mymap.addControl(new L.Control.Fullscreen());

      // $.ajax({
      //   url: "./data",
      //   type: "get",
      //   dataType: 'json',
      //   success: function (msg, status, jqXHR){
      //     $.each(msg, function(i,obj){
      //       var marker = L.marker([msg[i].latitude,msg[i].longitude],{title:"marker-"+i})
      //         .addTo(mymap)
      //         .bindPopup("<table>" +
      //         "<tr><td>Kecamatan</td><td>: " + msg[i].nama_kecamatan + "</td>" +
      //         "<tr><td>Desa</td><td>: " + msg[i].nama_desa + "</td> " +
      //         "<tr><td>Banjar</td><td>: " + msg[i].banjar + "</td>" +
      //         "<tr><td>Lokasi</td><td>: " + msg[i].lokasi + "</td>"+
      //         "<tr><td>Status</td><td>: " + msg[i].status + "</td>"+
      //         "</table>" +
      //         "<br><br><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal' onClick='document.getElementById(`titleModal`).innerHTML = `" + msg[i].lokasi + "`' style='font-size: small;'>Live Streaming</button>")
      //         .on('click', clickZoom);

      //       markers.push(marker);
      //     });
      //   }
      // });
      
      function clickZoom(e) {
        mymap.setView(e.target.getLatLng(), 14);
      }
        
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    });

    function adi(id,lat,lng,lokasi){
      $('#exampleModal2').modal('toggle');
      mymap.setView(new L.LatLng(lat,lng), 14);
      markers[id].openPopup();
    }
  </script>

</body>

</html>