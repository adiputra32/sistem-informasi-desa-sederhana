
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
  
    <title>Desa Kuwum</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">
  
    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">
  
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/venobox button-menu/venobox button-menu.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
  
    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
  
    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> 

    <script src="script/modernizr.custom.js"></script>

    <style>
        body{
            background-image: url('/assets/img/intro-bg.jpg');
        }

        .content{
            padding-left: 200px;
            padding-right: 200px;
        }

        .judul{
            margin-top: 50px;
            margin-bottom: 80px;
            text-align: center;
        }
        
        .menus{
            margin: auto;
        }

        .button-menu  {
            padding: 30px 0 20px 0;
            position: relative;
            overflow: hidden;
            border-radius: 10px;
            margin-bottom: 40px;
            background: #fff;
            box-shadow: 0 10px 29px 0 rgba(68, 88, 144, 0.1);
            transition: all 0.3s ease-in-out;
            text-align: center;  
        }

        .home{
            text-align: center;
            margin-top: 10        
        }

        .button-home{
            font-family: "Montserrat", sans-serif;
            font-size: 13px;
            font-weight: 600;
            text-transform: uppercase;
            letter-spacing: 1px;
            display: inline-block;
            padding: 10px 32px;
            border-radius: 4px;
            transition: 0.5s;
            color: #fff;
            background: #1bb1dc;
            margin-bottom: 50px;
            margin-top: 80px;
        }

        .footer{
            width: 100%;
            text-align: center;
        }

        .icon{
            margin: 0 auto 15px auto;
            padding-top: 12px;
            display: inline-block;
            text-align: center;
            border-radius: 50%;
            width: 60px;
            height: 60px;
        }

    </style>
 
</head>
    <div class="content">
        <div class="judul">
            <h2>WEBSITE RESMI</h2>
            <h1>PEMERINTAH DESA KUWUM</h1>
            <h2>adifd.000webhostapp.com</h2>
        </div>

        <div class="menu">
            <div class="row menus">
                <div class="col-lg-4">
                    <div class="button-menu" style="cursor: pointer;" onclick="windows.location.href='#'">
                        <div class="icon" style="background-color: #fceef3;"><i class="fas fa-store" style="color: #ff689b; font-size: 36px;"></i></div>
                        <br>
                        <h5>Layanan <br> Promosi Warga</h5>
                    </div>
                </div>
                
                <div class="col-lg-4">
                    <div class="button-menu" style="cursor: pointer;" onclick="windows.location.href='#'">
                        <div class="icon" style="background-color: #fff0da;"><i class="fas fa-user-md" style="color: #e98e06; font-size: 36px;"></i></div>
                        <br>
                        <h5>Layanan <br> Kesehatan</h5>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="button-menu" style="cursor: pointer;" onclick="windows.location.href='#'">
                        <div class="icon" style="background-color: #e6fdfc;"><i class="fas fa-baby" style="color: #3fcdc7; font-size: 36px;"></i></div>
                        <br>
                        <h5>Layanan <br> Posyandu</h5>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="button-menu" style="cursor: pointer;" onclick="windows.location.href='#'">
                        <div class="icon" style="background-color: #eafde7;"><i class="fas fa-newspaper" style="color: #41cf2e; font-size: 36px;"></i></div>
                        <br>
                        <h5>Layanan <br> Informasi Publik</h5>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="button-menu" style="cursor: pointer;" onclick="windows.location.href='#'">
                        <div class="icon" style="background-color: #e1eeff;"><i class="fas fa-file-alt" style="color: #2282ff; font-size: 36px;"></i></div>
                        <br>
                        <h5>Layanan <br> Surat-Surat</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="home">
            <a href="/" class="btn button-home">Lihat Tampilan Lengkap</a>
        </div>
    </div>

    <div class="footer">
        <p>Copyright © 2010 - 2019 Pemerintah Kabupaten Tabanan. All Right Reserved.</p>
    </div>

<body>
    
</body>
</html>
