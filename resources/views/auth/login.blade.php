<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desa Kuwum - Login</title>

    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

        .login-page {
            width: 360px;
            padding: 8% 0 0;
            margin: auto;
        }

        .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 360px;
            margin: 0 auto 100px;
            padding: 45px;
            text-align: center;
            box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
        }

        .form input {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form select {
            font-family: "Roboto", sans-serif;
            outline: 0;
            background: #f2f2f2;
            width: 100%;
            border: 0;
            margin: 0 0 15px;
            padding: 15px;
            box-sizing: border-box;
            font-size: 14px;
        }

        .form button {
            font-family: "Roboto", sans-serif;
            text-transform: uppercase;
            outline: 0;
            background: #1bb1dc;
            width: 100%;
            border: 0;
            padding: 15px;
            color: #FFFFFF;
            font-size: 14px;
            -webkit-transition: all 0.3 ease;
            transition: all 0.3 ease;
            cursor: pointer;
            border-radius: 4px;
        }

        .form button:hover,.form button:active,.form button:focus {
            background: #007bff;
        }

        .form .message {
            margin: 15px 0 0;
            color: #b3b3b3;
            font-size: 12px;
        }

        .form .message a {
            color: #1bb1dc;
            text-decoration: none;
        }

        .form .register-form {
            display: none;
        }

        .container {
            position: relative;
            z-index: 1;
            max-width: 300px;
            margin: 0 auto;
        }

        .container:before, .container:after {
            content: "";
            display: block;
            clear: both;
        }

        .container .info {
            margin: 50px auto;
            text-align: center;
        }

        .container .info h1 {
            margin: 0 0 15px;
            padding: 0;
            font-size: 36px;
            font-weight: 300;
            color: #1a1a1a;
        }

        .container .info span {
            color: #4d4d4d;
            font-size: 12px;
        }

        .container .info span a {
            color: #000000;
            text-decoration: none;
        }

        .container .info span .fa {
            color: #EF3B3A;
        }

        body {
            background-image: url('/assets/img/intro-bg.jpg'); /* fallback for old browsers */
            /* background: -webkit-linear-gradient(right, #76b852, #8DC26F);
            background: -moz-linear-gradient(right, #76b852, #8DC26F);
            background: -o-linear-gradient(right, #76b852, #8DC26F);
            background: linear-gradient(to left, #76b852, #8DC26F); */
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;      
        }

    </style>
</head>
<body>
    <div class="login-page">
        <div class="form">
            <center><h2>Desa Kuwum</h2></center>
            <form class="register-form">
                <center><h3>Register</h3></center>
                <br>
                <span style="float: left; margin-bottom: 15px;">Nama</span>
                <input type="text" placeholder="nama"/>

                <span style="float: left; margin-bottom: 15px;">Username</span>
                <input type="text" placeholder="username"/>

                <span style="float: left; margin-bottom: 15px;">Nomor Telepon</span>
                <input type="number" placeholder="nomor telepon"/>

                <span style="float: left; margin-bottom: 15px;">Password</span>
                <input type="password" placeholder="password"/>

                <span style="float: left; margin-bottom: 15px;">Konfirmasi Password</span>
                <input type="password" placeholder="konfirmasi password"/>

                <button>Register</button>
                <p class="message">Sudah memiliki akun? <a href="#">Login</a></p>
            </form>

            <form class="login-form">
                <center><h3>Login</h3></center>
                <br>
                <span style="float: left; margin-bottom: 15px;">Username</span>
                <input type="text" placeholder="username"/>
                
                <span style="float: left; margin-bottom: 15px;">Password</span>
                <input type="password" placeholder="password"/>
                
                <span style="float: left; margin-bottom: 15px;">Login Sebagai</span>
                <select name="role" id="role">
                    <option value="warga">Warga</option>
                    <option value="dokter">Dokter</option>
                    <option value="admin">Admin</option>
                </select>

                <button>login</button>
                <p class="message">Belum memiliki akun? <a href="#">Register</a></p>
            </form>

        </div>
    </div>

    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.message a').click(function(){
                $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
            });
        });
    </script>
</body>

</html>